API reference
=============

.. toctree::
   :maxdepth: 4

   Tagger
   TaggingData
   TaggerCollection

   save_calibration
   calibration_functions
   resolution_model
   apply_tagger
   plotting
   link_functions
   ToyDataGenerator
   CalParameters
   GlobalOptions
