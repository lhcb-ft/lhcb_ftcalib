Standard plots
==============
The plotting module provides components for assembling the standard
plots a TaggerCollection module would create by calling the draw_calibration_curve function.

.. autofunction:: lhcb_ftcalib.draw_calibration_curve

Methods called by draw_calibration_curve:
-----------------------------------------

.. autofunction:: lhcb_ftcalib.plotting.eta_omega_binning
.. autofunction:: lhcb_ftcalib.plotting.calibration_lineshape
.. autofunction:: lhcb_ftcalib.plotting.confidence_bands_lineshape
.. autofunction:: lhcb_ftcalib.plotting.print_fit_setup
.. autofunction:: lhcb_ftcalib.plotting.get_eta_plotrange
.. autofunction:: lhcb_ftcalib.plotting.get_omega_plotrange
