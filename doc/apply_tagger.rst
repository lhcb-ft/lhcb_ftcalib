Applying calibrations
=====================

.. autoclass:: lhcb_ftcalib.TargetTagger
   :members:
   :undoc-members:
   :show-inheritance:

.. autoclass:: lhcb_ftcalib.TargetTaggerCollection
   :members:
   :undoc-members:
   :show-inheritance:
